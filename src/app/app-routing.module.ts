import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FlightListComponent } from './flight-list/flight-list.component';
import { FlightBookingComponent } from './flight-booking/flight-booking.component';

const routes: Routes = [
{ path: 'flight-booking', component: FlightBookingComponent },
{ path: 'booking', component: FlightBookingComponent },
{ path: 'home', component: FlightBookingComponent },
{ path: 'list', component: FlightListComponent },
{ path: 'website/:id', component: FlightBookingComponent },
{ path: 'design/:id', component: FlightBookingComponent },
{path : '', component : FlightBookingComponent}

];

@NgModule({
  imports: [
  
RouterModule.forRoot(routes),
    CommonModule
  ],
  
  
exports: [

RouterModule

],
  
  declarations: []
})
export class AppRoutingModule { }
